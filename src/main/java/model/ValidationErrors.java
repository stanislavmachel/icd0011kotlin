package model;

import lombok.Getter;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ValidationErrors {

    @Getter
    private final List<ValidationError> errors = new ArrayList<>();

    public void addFieldError(FieldError fieldError) {
        if (fieldError.getArguments() == null) {
            throw new IllegalArgumentException("arguments array should not be null");
        } else if (fieldError.getCodes() == null || fieldError.getCodes().length < 1) {
            throw new IllegalArgumentException("error without a code");
        }

        List<String> args = Stream.of(fieldError.getArguments())
                .filter(arg -> !(arg instanceof DefaultMessageSourceResolvable))
                .map(String::valueOf)
                .collect(Collectors.toList());

        errors.add(new ValidationError(fieldError.getCodes()[0], args));
    }
}
