package samples.dto

class Client {
    var name: String = ""
    set(name) {
        println("setting name to: $name")

        field = name
    }
    get() {
        println("reading name")

        return field
    }
}

fun main() {

    val c = Client()

    c.name = "Alice"

    println(c.name)

}