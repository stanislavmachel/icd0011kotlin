package samples.dto

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

data class Person(val name: String)

fun main() {

    val p = Person("Alice")

    println(p)

    val mapper = ObjectMapper().registerKotlinModule()

    val json = mapper.writeValueAsString(p)

    println(json)

    val p2 = mapper.readValue(json, Person::class.java)

    println(p2)

}