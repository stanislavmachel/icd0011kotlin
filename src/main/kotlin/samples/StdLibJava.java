package samples;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class StdLibJava {

    @Test
    public void streams() {

        var list = List.of(1, 2, 3, 4)
                .stream()
                .filter(x -> x % 2 == 0)
                .map(x -> x * 2)
                .collect(Collectors.toList());


        System.out.println(list);
    }

    @Test
    public void collections() {

        var list = new ArrayList<>(List.of(1, 2, 3));

        list.add(4);

        var last = list.get(list.size() - 1);

        System.out.println(last);

        var min = list.stream().min(Comparator.naturalOrder());

        System.out.println(min);

        var sum = list.stream().reduce(Integer::sum);

        System.out.println(sum);

        var total = getOrders().stream()
                .mapToDouble(o -> o.getPrice() * o.getQuantity())
                .sum();

        System.out.println(total);
    }

    @Test
    public void orEmpty() {
        var list = new ArrayList<Integer>();

        if (list != null) {
            list.forEach(System.out::println);
        }
    }

    @Test
    public void chunked() {

        byte[] bytes = hexStringToBytes("0B2A");

        assertThat(bytes.length, is(2));
        assertThat(Byte.toUnsignedInt(bytes[0]), is(11));
        assertThat(Byte.toUnsignedInt(bytes[1]), is(42));
    }


    private byte[] hexStringToBytes(String hash) {
        byte[] bytes = new byte[hash.length() / 2];
        for (int j = 0; j < hash.length(); j += 2) {
            int firstDigit = Character.digit(hash.charAt(j), 16);
            int secondDigit = Character.digit(hash.charAt(j + 1), 16);
            bytes[j / 2] = Integer.valueOf((firstDigit * 16) + secondDigit).byteValue();
        }
        return bytes;
    }





















    private List<Order> getOrders() {
        return List.of(new Order(2.5, 3), new Order(3.5, 2));
    }
}
