package samples

import org.junit.Test

class TypesKotlin {

    @Test
    fun notNullableTypes() {
        var x = 1

        // x = null;

        val list = ArrayList<Int>()

        // list.add(null)
    }



    
    @Test
    fun nullableTypes() {
        var x : Int? = 1

        x = null

        val list = ArrayList<Int?>()

        list.add(null)

        x = list[0]

        // val y : Int = list[0]
    }



    @Test
    fun nullCheck() {
        var x : Int? = 1
        var y : Int? = 1

        if (x == null || y == null) {
            return
        }

        var z = x + y
    }

    @Test
    fun typeConversion() {
        val x : Int? = getValueFromExternalSource()

        val nonNullX = x ?: throw IllegalStateException("x should not be null")

        // ...
    }














    private fun getValueFromExternalSource(): Int? {
        return 0
    }


}

