package samples

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Assert.assertFalse

import org.junit.Test
import samples.OperatorsJava.*

class OperatorsKotlin {

    @Test
    fun equals() {
        val a : Int? = 128
        val b : Int? = 128

        assertTrue(a == b)

        assertFalse(a === b)
    }

    @Test
    fun indexedAccess() {
        val list = listOf(1, 2)

        assertThat(list[0], equalTo(1))
        assertThat(list[1], equalTo(2))
    }

    @Test
    fun safeCall() {
        assertThat(getWorker().manager.name, equalTo("Alice"))

        assertThat(getNothing()?.manager?.name, equalTo(null))
    }

    @Test
    fun elvis() {
        val a: String? = null

        val b = a ?: ""

        assertThat(b, equalTo(""))
    }

    @Test
    fun safeCast() {
        val a: Number = 1L

        val b = a as? Byte

        assertThat(b, equalTo(null))
    }

    override fun equals(obj: Any?): Boolean {

        val other = obj as? Employee ?: return false

        // check fields
        return true
    }

}