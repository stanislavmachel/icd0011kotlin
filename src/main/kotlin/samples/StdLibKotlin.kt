package samples

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class StdLibKotlin {

    @Test
    fun streams() {

        val list = listOf(1, 2, 3, 4)
            .filter { it % 2 == 0 }
            .map { it * 2 }

        println(list)
    }

    @Test
    fun collections() {
        val list = arrayListOf(1, 2, 3)

        list.add(4)

        println(list.last())

        val min = list.minOrNull()

        println(min)

        val sum = list.sum()

        println(sum)

        val total = getOrders().sumOf { it.price * it.quantity }

        println(total)
    }

    @Test
    fun orEmpty() {
        val list : List<Int>? = null

        list.orEmpty().forEach { print(it) }
    }

    @Test
    fun chunked() {
        val bytes = hexStringToBytes("0B2A")

        assertThat(bytes.size, equalTo(2))
        assertThat(bytes[0].toInt(), equalTo(17))
        assertThat(bytes[1].toInt(), equalTo(34))
    }

    private fun hexStringToBytes(hash: String) : ByteArray =
        hash.chunked(2).map { it.toInt(16).toByte() }.toByteArray()





















    private fun getOrders() = listOf(Order(2.5, 3), Order(3.5, 2))

}

class Order(val price: Double, val quantity: Int)


