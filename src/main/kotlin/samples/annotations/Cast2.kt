package samples.annotations

import java.lang.RuntimeException

fun main() {

    val annotationType: Class<out Annotation> = Delete::class.java

    for (method in Annotated().javaClass.declaredMethods) {

        val annotation = method.getAnnotation(annotationType) ?: continue

        println(getAnnotationValue(annotation))
    }

}

fun getAnnotationValue(annotation: Annotation): String {
    return when (annotation) {
        is Get -> annotation.value
        is Post -> annotation.value
        is Delete -> annotation.value
        else -> throw RuntimeException("unexpected type")
    }
}
