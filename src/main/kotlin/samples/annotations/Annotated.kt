package samples.annotations

class Annotated {

    @Get("get path")
    fun get() {}

    @Post("post path")
    fun save(sth: Any) {}

    @Delete("delete path")
    fun delete(id: Long) {}

}

