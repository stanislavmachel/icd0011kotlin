package samples;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static samples.CardValue.*;

public class SyntaxJava {

    @Test
    public void multilineString() {
        var string = "line1\n"
                   + "line2";

        assertThat(string, equalTo("line1\nline2"));

        int time = 1;

        string = "Time: %s sec".formatted(time);

        assertThat(string, equalTo("Time: 1 sec"));
    }

    @Test
    public void loops() {

        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }

        for (int i = 0; i <= 9; i += 2) {
            System.out.println(i);
        }

        var map = Map.of(1, "one", 2, "two");

        for (var entry: map.entrySet()) {
            System.out.printf("%s -> %s%n", entry.getKey(), entry.getValue());
        }

        int index = 0;
        for (var value: List.of(1, 2)) {
            System.out.printf("%s -> %s%n", index++, value);
        }
    }

    @Test
    public void ranges() {
        var x = 3;

        if (x >= 0 && x <= 6) {
            System.out.println("x is between 0 and 6");
        }

        var cardValue = S4;

        if (cardValue.ordinal() >= S2.ordinal()
                && cardValue.ordinal() <= S10.ordinal()) {

            System.out.println("spot card");
        }
    }

    @Test
    public void switching() {
        var data = 1;

        switch (data) {
            case 1:
            case 2:
                System.out.println("one or two");
                break;
            case 3:
                System.out.println("three");
                break;
            default:
                System.out.println("unknown");
        }
    }

    @Test
    public void dataClasses() {
        var t1 = new TaskJava("NEW");
        var t2 = new TaskJava();

        System.out.println(t1.getStatus());

        System.out.println(t1.equals(t2));
    }

    @Test
    public void exceptions() {
        FileInputStream reader = null;

        try {
            reader = new FileInputStream("");

            // ...

        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {}
            }
        }
    }

    @Test
    public void bigDecimal() {

        // (n * (n + 1)) / 2

        var n = BigDecimal.valueOf(10);

        var result = n.multiply(n.add(BigDecimal.valueOf(1))).divide(BigDecimal.valueOf(2));

        System.out.println(result);
    }

}

