package samples;

import java.util.Objects;

public class TaskJava {

    private String status;

    public TaskJava() {
        status = "NEW";
    }

    public TaskJava(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof TaskJava taskJava)) {
            return false;
        }

        return Objects.equals(status, taskJava.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status);
    }
}
