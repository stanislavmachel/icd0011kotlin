package samples;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class OperatorsJava {

    @Test
    public void equals() {
        Integer a = 128;
        Integer b = 128;

        assertTrue(a.equals(b));

        assertFalse(a == b);
    }

    @Test
    public void indexedAccess() {
        var list = List.of(1, 2);

        assertThat(list.get(0), equalTo(1));
        assertThat(list.get(1), equalTo(2));
    }

    @Test
    public void safeCall() {
        assertThat(getWorker().manager.name, equalTo("Alice"));

        Employee worker = getNothing();
        Employee manager = worker == null ? null : worker.manager;
        String managerName = manager == null ? null : manager.name;

        assertThat(managerName, equalTo(null));
    }

    @Test
    public void elvis() {
        String a = null;

        String b = a != null ? a : "";

        assertThat(b, equalTo(""));
    }

    @Test
    public void safeCast() {
        Number a = 1L;

        Number b = a instanceof Byte ? (Byte) a : null;

        assertThat(b, equalTo(null));
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Employee)) {
            return false;
        }

        Employee other = (Employee) obj;

        // check fields

        return true;
    }

    static class Employee {
        public Employee(String name) {
            this.name = name;
        }

        public String name;
        public Employee manager;
    }

    public static Employee getWorker() {
        Employee alice = new Employee("Alice");
        Employee bob = new Employee("Bob");
        bob.manager = alice;
        return bob;
    }

    public static Employee getManager() {
        return getWorker().manager;
    }

    public static Employee getNothing() {
        return null;
    }

}
