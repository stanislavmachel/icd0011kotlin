package samples

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import samples.CardValue.S10
import samples.CardValue.S2
import java.io.FileInputStream
import java.math.BigDecimal
import java.util.*

class SyntaxKotlin {

    @Test
    fun multilineString() {
        val string = """
            line1
            line2""".trimIndent()

        assertThat(string, equalTo("line1\nline2"))

        val time = 1

        assertThat("Time: $time sec", equalTo("Time: 1 sec"))
    }

    @Test
    fun loops() {

        for(i in 0..9) {
            println(i)
        }

        for(i in 0 until 10 step 2) {
            println(i)
        }

        val map = mapOf(1 to "one", 2 to "two")

        for ((key, value) in map) {
            println("$key -> $value")
        }

        for ((index, value) in listOf(1, 2).withIndex()) {
            println("$index -> $value")
        }
    }

    @Test
    fun ranges() {
        val x = 3

        if (x in 0..6) {
            println("x is between 0 and 6")
        }

        val cardValue = CardValue.S4

        if (cardValue in S2..S10) {
            println("spot card")
        }
    }

    @Test
    fun switching() {
        val data : Number = 1
        when (data) {
            1, 2 -> println("one or two")
            3 -> println("three")
            else -> println("unknown")
        }

        val result = when (data) {
            1 -> "one"
            2 -> "two"
            else -> "unknown"
        }

        println(result)

        val name = when(val c = getCustomer()) {
            is PrivateCustomer -> c.firstName
            is Company -> c.companyName
            else -> "unknown"
        }

        println(name)
    }

    @Test
    fun dataClasses() {
        val t1 = TaskKotlin("NEW")
        val t2 = TaskKotlin()

        println(t1.status)

        println(t1 == t2)
    }

    @Test
    fun exceptions() {
        var reader: FileInputStream? = null

        try {
            reader = FileInputStream("")

            // ...

        } finally {
            reader?.close()
        }
    }

    @Test
    fun bigDecimal() {

        // (n * (n + 1)) / 2

        val n = BigDecimal.valueOf(10)

        val result = (n * (n + BigDecimal(1))) / BigDecimal(2)

        print(result)

    }

    @Test
    fun customExtensions() {

        fun String.myUppercase() = this.uppercase(Locale.getDefault())

        println("abc".myUppercase())
    }

    @Test
    fun operatorOverloading() {

        val oneEuro = Money(1, "EUR")

        println(oneEuro.times(2))

        println(oneEuro * 2)

    }

    class Money(val amount: Int, val currency: String) {
        override fun toString(): String {
            return "$amount $currency"
        }
    }

    operator fun Money.times(multiplier: Int): Money
            = Money(amount * multiplier, currency)

    private fun getCustomer() : Customer = Company("x")
}

open class Customer

class PrivateCustomer(val firstName: String) : Customer()

class Company(val companyName: String) : Customer()

enum class CardValue {
    S2, S3, S4, S5, S6, S7, S8, S9, S10, J, Q, K, A
}
